const ExtractTextPlugin = require('extract-text-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const PurifyCSSPlugin = require('purifycss-webpack-plugin');

exports.setup = function (paths) {
    return {
        module: {
            loaders: [
                {
                    test: /\.s?css$/,
                    loaders: ['style', 'css','sass'],
                    include: paths
                }
            ]
        }
    };
}

exports.purify = function(paths) {
  return {
    plugins: [
      new PurifyCSSPlugin({
        basePath: process.cwd(),
        paths: paths,
        purifyOptions: {info: true}
      }),
    ]
  }
}

exports.extract = function(paths) {
  return {
    module: {
      loaders: [
        {
          test: /\.s?css$/,
          loader: ExtractTextPlugin.extract('style', 'css','sass'),
          include: paths
        }
      ]
    },
    plugins: [
      new ExtractTextPlugin('[name].[chunkhash].css'),
      new OptimizeCssAssetsPlugin({cssProcessorOptions: { discardComments: {removeAll: true } }})
    ]
  };
}