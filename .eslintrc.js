module.exports = {
    "env": {
        "browser": true,
        "commonjs": true,
        "es6": true
    },
    "extends": "eslint:recommended",
    "installedESLint": true,
    "no-process-env" : false,
    "parserOptions": {
        "ecmaFeatures": {
            "experimentalObjectRestSpread": true,
            "jsx": true
        },
        "sourceType": "module"
    },
    "plugins": [
        "react"
    ],
    "rules": {
        "no-console" : "off",
        "linebreak-style": [
            "error",
            "unix"
        ],
    },
    "globals":{
        process: false,
        console: false,
        __dirname: false
    }
};