### What is this repository for? ###

* quick and dirty demo of seed project for react using webpack as build tool

### How do I get set up? ###

```
git clone git@bitbucket.org:codeonfire/webpack-react.git
cd webpack-react
npm install
npm start
```