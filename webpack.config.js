const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const merge = require('webpack-merge')
const validate = require('webpack-validator')

const PATHS = {
    css: [path.join(__dirname, 'static', 'common.css'), path.join(__dirname, 'sass', 'common.scss')],
    app: path.join(__dirname, 'src', 'app'),
    build: path.join(__dirname, 'dist')
}


const common = {
    entry: {
        css: PATHS.css,
        app: PATHS.app
    },
    output: {
        path: PATHS.build,
        filename: '[name].js'
    },

    module: {
        preLoaders:[
            {
                test:/\.js$/,
                include: ['./src/**/*'],
                loader: 'jshint-loader',

            }
        ],
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: ['node_modules'],
                loader: 'babel-loader',
                query: {
                    presets: ['babel-preset-react', 'babel-preset-es2015'].map(require.resolve)
                }
            },
            {
                test: /\.hbs$/,
                loader: 'handlebars-loader'
            }
        ]
    },
    resolve: {
        extensions: ['', '.js', '.jsx']
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'Webpack demo',
            template: 'views/index.hbs'
        })
    ]


}


var config;
const build = require('./webpack/build')
const devServer=require('./webpack/devServer');
const css=require('./webpack/css');
const script=require('./webpack/script');

switch (process.env.npm_lifecycle_event) {
    case 'stats':
        config = merge(common,
            {

            }
        )
        break;
    case 'build':
        config = merge(
            common,
            css.extract(PATHS.css),
            {
                output: {
                    path: PATHS.build,
                    filename: '[name].[chunkhash].js',
                    chunkFilename: '[chunkhash].js'
                },
                resolve:{
                    alias:{
                        'react' : 'react-lite',
                        'react-dom' : 'react-lite'
                    }
                }
            },
            build.clean(PATHS.build),
            script.minify(),
            build.extractBundle({ name: 'vendor', entries: ['react', 'react-dom','radium'] }),
            css.purify()
        );
        break;
    default:
        config = merge(common,
            css.setup(PATHS.css),
            devServer.setup({
                // Customize host/port here if needed
                host: process.env.HOST,
                port: process.env.PORT
            }),
            {
                devtool: 'eval-source-map'
            }
        );
}

module.exports = validate(config, { quiet: (process.env.npm_lifecycle_event == 'test') });
