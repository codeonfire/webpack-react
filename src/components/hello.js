import React from "react";
import Radium from "radium";


class Hello extends React.Component {
    render() {
        return (
            <div>
                Hello, {this.props.name} !
            </div>
        );
    }
}

export default Radium(Hello)