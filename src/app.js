var config = require('../config/index')
import React from "react";
import ReactDOM from "react-dom";
import Hello from "./components/hello";

ReactDOM.render(
  <Hello name="My New World goes here" />,
  document.getElementById('react_root')
);